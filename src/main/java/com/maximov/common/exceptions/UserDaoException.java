package com.maximov.common.exceptions;

/**
 * Created by maxim_000 on 23.02.2017.
 */
public class UserDaoException extends Exception {
    public UserDaoException(Throwable e) {
        super(e);
    }
}
