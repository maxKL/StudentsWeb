package com.maximov.services;

import com.maximov.models.dao.LectionDAO;
import com.maximov.models.pojo.Lection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LectionService {
    private LectionDAO lectionDAO;

    @Autowired
    public LectionService(LectionDAO lectionDAO) {
        this.lectionDAO = lectionDAO;
    }


    public List<Lection>  getAll(){
        return this.lectionDAO.getList();
    }

    public Lection getById(int id){
        return this.lectionDAO.getById(id);
    }

    public void create(Lection student){
        this.lectionDAO.create(student);
    }

    public void update(Lection student){
        this.lectionDAO.update(student);
    }

    public boolean delete(int id){
        return this.lectionDAO.delete(id);
    }
}
