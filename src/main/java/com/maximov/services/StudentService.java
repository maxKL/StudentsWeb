package com.maximov.services;

import com.maximov.models.dao.LectionDAO;
import com.maximov.models.dao.StudentDAO;
import com.maximov.models.pojo.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentService {
    private StudentDAO studentDAO;

    @Autowired
    public StudentService(StudentDAO studentDAO) {
        this.studentDAO = studentDAO;
    }

    public Iterable<Student>  getAllStudents(){
        return this.studentDAO.findAll();
    }

    public Student getById(int id){
        return this.studentDAO.findOne(id);
    }

    public void create(Student student){
        this.studentDAO.save(student);
    }

    public void update(Student student){
        this.studentDAO.save(student);
    }

    public void delete(int id){
         this.studentDAO.delete(id);
    }
}
