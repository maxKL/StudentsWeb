package com.maximov.models.pojo;

import javax.persistence.*;

@Entity
@Table(name = "user", schema = "public")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Users_Seq")
    @SequenceGenerator(name = "Users_Seq", sequenceName = "user_id_seq", allocationSize = 1)
    private  Integer id;

    @Column(name = "login")
    private  String login;

    @Column(name = "password")
    private  String password;

    @Column(name = "role")
    private  String role;

    @Column(name = "email")
    private  String email;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
