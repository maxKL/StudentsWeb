package com.maximov.models.connector;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Connector {
    private static String url = "jdbc:postgresql://localhost:5432/StudentsDB";
    private static String login = "postgres";
    private static String password = "08051992";
    private static Connection conn;


    private Connector() {
    }

    /**
     * Получить подключение
     * @return Подключение к БД
     */
    public static synchronized Connection getConnection() throws ClassNotFoundException, SQLException {
        //if(conn == null){
            Class.forName("org.postgresql.Driver");
            conn = DriverManager.getConnection(url, login, password);
        //}

        return conn;
    }
}
