package com.maximov.models.dao;

import com.maximov.models.pojo.User;
import org.springframework.data.repository.CrudRepository;


public interface UserDAO extends CrudRepository<User, Integer>{

    User findByLogin(String login);
}
