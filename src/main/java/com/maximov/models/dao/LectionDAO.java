package com.maximov.models.dao;

import com.maximov.models.connector.Connector;
import com.maximov.models.pojo.Lection;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class LectionDAO {
    private static String SELECT_ALL = "SELECT * FROM public.lection";
    private static String DELETE_BY_ID = "DELETE FROM public.lection WHERE id = ?";
    private static final String SQL_FIND_BY_ID = "SELECT * FROM public.lection where id = ?";
    private static final String SQL_CREATE =
            "INSERT INTO public.lection (name, text, subject, date, group_id) VALUES (?, ?, ?, ?, ?)";
    private static final String SQL_UPDATE =
            "UPDATE public.lection SET name = ?, text = ?, subject = ? WHERE id = ?";

    private static Logger logger = Logger.getLogger(LectionDAO.class);

    public List<Lection> getList(){
        List<Lection> lections = new ArrayList<>();
        try (Connection connection = Connector.getConnection()){
            Statement statement = connection.createStatement();

            ResultSet resultSet = statement.executeQuery(SELECT_ALL);

            while (resultSet.next()){
                Lection lection = new Lection();
                lection.setId(resultSet.getInt("id"));
                lection.setName(resultSet.getString("name"));
                lection.setText(resultSet.getString("text"));
                lection.setSubject(resultSet.getString("subject"));
                lections.add(lection);
            }
        }
        catch (SQLException | ClassNotFoundException e) {
            logger.error(e);
        }
        return lections;
    }

    public Lection getById(int id){
        Lection lection = new Lection();
        try (Connection connection = Connector.getConnection()){
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_BY_ID);
            preparedStatement.setInt( 1, id );

            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            lection.setId(resultSet.getInt("id"));
            lection.setName(resultSet.getString("name"));
            lection.setText(resultSet.getString("text"));
            lection.setSubject(resultSet.getString("subject"));
        }
        catch (SQLException | ClassNotFoundException e) {
            logger.error(e);
        }
        return lection;
    }


    public void create(Lection lection){
        try (Connection connection = Connector.getConnection()){
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_CREATE);

            preparedStatement.setString(1, lection.getName());
            preparedStatement.setString(2, lection.getText());
            preparedStatement.setString(3, lection.getSubject());
            preparedStatement.setTimestamp(4, Timestamp.valueOf(lection.getDate()));
            preparedStatement.setInt(5, lection.getGroupId());

            preparedStatement.executeUpdate();
        }
        catch (SQLException | ClassNotFoundException e) {
            logger.error(e);
        }
    }

    public void update(Lection lection){
        try (Connection connection = Connector.getConnection()){
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE);

            preparedStatement.setString(1, lection.getName());
            preparedStatement.setString(2, lection.getText());
            preparedStatement.setString(3, lection.getSubject());
            preparedStatement.setInt(4, lection.getId());

            preparedStatement.executeUpdate();
        }
        catch (SQLException | ClassNotFoundException e) {
            logger.error(e);
        }
    }

    public boolean delete(int id){
        try (Connection connection = Connector.getConnection()){
            PreparedStatement statement = connection.prepareStatement(DELETE_BY_ID);
            statement.setInt(1, id);
            return statement.executeUpdate() > 0;
        }
        catch (SQLException | ClassNotFoundException e) {
            logger.error(e);
        }
        return false;
    }
}
