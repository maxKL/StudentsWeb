package com.maximov.models.dao;

import com.maximov.models.connector.Connector;
import com.maximov.models.pojo.Student;
import com.maximov.models.pojo.User;
import org.apache.log4j.Logger;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public interface StudentDAO extends CrudRepository<Student, Integer> {

}
