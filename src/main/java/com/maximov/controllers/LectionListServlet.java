package com.maximov.controllers;

import com.maximov.models.pojo.Lection;
import com.maximov.services.LectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


@WebServlet(name = "LectionListServlet", urlPatterns = {"/lectionList"})
public class LectionListServlet extends HttpServlet {
    private LectionService service;

    @Autowired
    public void setService(LectionService service) {
        this.service = service;
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, config.getServletContext());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Lection> lections = this.service.getAll();
        req.setAttribute("lections", lections);

        req.getRequestDispatcher("/lectionList.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = req.getParameter("action");
        String id = req.getParameter("id");
        switch (action){
            case "delete":
                this.service.delete(Integer.parseInt(id));
                break;
        }
    }
}
