package com.maximov.controllers;

import com.maximov.common.exceptions.UserDaoException;
import com.maximov.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class RegistrationController {
    private UserService service;

    @Autowired
    public void setService(UserService service) {
        this.service = service;
    }

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String showLoginPage() {
        return "registration";
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public ModelAndView doPost(@RequestParam(name = "login") String login,
                               @RequestParam(name = "password") String password) {
        try {
            service.registrationUser(login, password);

            return new ModelAndView("redirect:/login");
        } catch (UserDaoException e) {
            return new ModelAndView("redirect:/error");
        }
    }
}
