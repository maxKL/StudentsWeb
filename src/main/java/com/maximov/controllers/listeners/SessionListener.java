package com.maximov.controllers.listeners;

import com.maximov.common.utils.mailServer.MailServer;
import com.maximov.models.pojo.User;
import org.apache.log4j.Logger;
import com.maximov.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.mail.MessagingException;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.IOException;

public class SessionListener implements HttpSessionListener, HttpSessionAttributeListener, ServletContextListener {
    private Logger logger = Logger.getLogger(SessionListener.class);

    private UserService service;

    @Autowired
    public void setService(UserService service) {
        this.service = service;
    }


    @Override
    public void contextInitialized(ServletContextEvent sce) {
        WebApplicationContextUtils
                .getRequiredWebApplicationContext(sce.getServletContext())
                .getAutowireCapableBeanFactory()
                .autowireBean(this);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

    }

    @Override
    public void sessionCreated(HttpSessionEvent se) {
        logger.trace(se.getSession().getId());
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {

    }

    @Override
    public void attributeAdded(HttpSessionBindingEvent event) {
//        if(event.getName().equals("userid"));{
//            int userid = Integer.parseInt(String.valueOf(event.getValue()));
//            User user = service.getById(userid);
//            try {
//                MailServer mailServer = new MailServer();
//                mailServer.sendMessage(user.getEmail(), "LogIN", "You have logged in");
//
//            } catch (IOException | MessagingException e) {
//                logger.error(e);
//            }
//        }
    }

    @Override
    public void attributeRemoved(HttpSessionBindingEvent event) {

    }

    @Override
    public void attributeReplaced(HttpSessionBindingEvent event) {

    }
}
