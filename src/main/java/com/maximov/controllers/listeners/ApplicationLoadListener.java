package com.maximov.controllers.listeners;

import org.apache.log4j.Logger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class ApplicationLoadListener implements ServletContextListener {
    private Logger logger = Logger.getLogger(ApplicationLoadListener.class);

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        logger.trace("Site started");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        logger.trace("Site stoped");

    }
}
