package com.maximov.controllers;

import com.maximov.models.pojo.Student;
import com.maximov.services.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class StudentController {
    private StudentService service;

    @Autowired
    public void setService(StudentService service) {
        this.service = service;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String students(Model model){
        Iterable<Student> studentList = service.getAllStudents();
        model.addAttribute("students", studentList);

        return "students";
    }

    @RequestMapping(value = "/student", method = RequestMethod.GET)
    public String studentForm(Model model,
                  @RequestParam(name = "id", defaultValue = "0") String idParam){
        int id = idParam != null ? Integer.parseInt(idParam) : 0;

        Student student = new Student();
        if(id > 0)
            student = this.service.getById(id);

        model.addAttribute("student", student);

        return "studentForm";
    }

    @RequestMapping(value = "/student", method = RequestMethod.POST)
    public ModelAndView postStudent(Student student){
        if(student.getId() == 0) {
            student.setGroupId(1);
            this.service.create(student);
        } else{
            this.service.update(student);
        }

        return new ModelAndView("redirect:/");
    }

    @RequestMapping(value = "/student/delete", method = RequestMethod.POST)
    public void deleteStudent(int id){
        this.service.delete(id);
    }

}
