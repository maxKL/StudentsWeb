package com.maximov.controllers;

import com.maximov.models.pojo.Student;
import com.maximov.services.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

public class EditServlet extends HttpServlet {
    private StudentService service;

    @Autowired
    public void setService(StudentService service) {
        this.service = service;
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, config.getServletContext());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String idParam = req.getParameter("id");
        int id = idParam != null ? Integer.parseInt(idParam) : 0;

        Student student = new Student();
        if(id > 0)
            student = this.service.getById(id);

        req.setAttribute("student", student);
        req.getRequestDispatcher("/studentForm.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Student student = new Student();
        student.setId(Integer.parseInt(req.getParameter("id")));
        student.setName(req.getParameter("name"));

        String dateParam = req.getParameter("birthdate");
        if(!dateParam.equals("null"))
            student.setBirthdate(new Date(Date.parse(dateParam)));
        student.setSex(Boolean.parseBoolean(req.getParameter("sex")));
        student.setGroupId(Integer.parseInt(req.getParameter("groupId")));
        if(student.getId() == 0) {
            student.setGroupId(1);
            this.service.create(student);
        } else{
            this.service.update(student);
        }

        resp.sendRedirect("/students/list");
    }
}
