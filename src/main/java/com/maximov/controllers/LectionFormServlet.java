package com.maximov.controllers;

import com.maximov.models.pojo.Lection;
import com.maximov.services.LectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;

@WebServlet(name = "LectionFormServlet", urlPatterns = {"/lectionForm"})
public class LectionFormServlet extends HttpServlet {
    private LectionService service;

    @Autowired
    public void setService(LectionService service) {
        this.service = service;
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, config.getServletContext());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String idParam = req.getParameter("id");
        int id = idParam != null ? Integer.parseInt(idParam) : 0;

        Lection lection = new Lection();
        if(id > 0)
            lection = this.service.getById(id);

        req.setAttribute("lection", lection);
        req.getRequestDispatcher("/lectionForm.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");

        Lection lection = new Lection();
        lection.setId(Integer.parseInt(req.getParameter("id")));
        lection.setName(req.getParameter("name"));
        lection.setSubject(req.getParameter("subject"));
        lection.setText(req.getParameter("text"));
        lection.setDate(LocalDateTime.now());
        lection.setGroupId(1);
        if(lection.getId() == 0) {
            this.service.create(lection);
        } else{
            this.service.update(lection);
        }

        resp.sendRedirect("/students/lectionList");
    }
}
