<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: maxim_000
  Date: 24.02.2017
  Time: 10:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" session="false" %>
<html>
<head>
    <title>Lections</title>
</head>
<body>

<a href="list">Список студентов</a><br/>

<table>
    <tr>
        <td>Название</td>
        <td>Тема</td>
        <td>Текст</td>
        <td></td>
        <td></td>
    </tr>
    <c:forEach items="${lections}" var="lectionItem">
        <tr id="<c:out  value="${lectionItem.id}"></c:out>">
            <td><c:out value="${lectionItem.name}"></c:out></td>
            <td><c:out value="${lectionItem.subject}"></c:out></td>
            <td><c:out value="${lectionItem.text}"></c:out></td>
            <td><a href="" onclick="deleteLection(<c:out  value="${lectionItem.id}"></c:out>)">Удалить</a></td>
            <td><a href="lectionForm?id=<c:out  value="${lectionItem.id}"></c:out>">Редактировать</a></td>
        </tr>
    </c:forEach>
</table>
<a href="lectionForm?id=0">Создать</a>
</body>
<script type="text/javascript" src="js/vendor/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="js/lectionList.js"></script>
</html>
