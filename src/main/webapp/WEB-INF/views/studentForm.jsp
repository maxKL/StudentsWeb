<%@ page import="com.maximov.models.pojo.Student" %><%--
  Created by IntelliJ IDEA.
  User: maxim_000
  Date: 23.02.2017
  Time: 20:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Student</title>
</head>
<body>
<form action="${pageContext.request.contextPath}/student" method="post">

    <input hidden id="id" name="id" value="${student.id}"> <br/>
    <label for="name">Имя: </label> <input id="name" name="name" value="${student.name}"> <br/>
    <input id="birthdate" name="birthdate" type="date" value="${student.birthdate}"> <br/>
    <input hidden id="sex" name="sex" value="${student.sex}"> <br/>
    <input hidden id="groupId" name="groupId" value="${student.groupId}"> <br/>

    <input type="submit" value="Сохранить" />
</form>
</body>
</html>
