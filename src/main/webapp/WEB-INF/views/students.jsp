<%@ page import="com.maximov.models.pojo.Student" %>
<%@ taglib prefix="c"
           uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: maxim_000
  Date: 23.02.2017
  Time: 10:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page session="false" %>
<html>
<head>
    <title>List</title>
</head>
<body>
<h1>LIST</h1>
<form action="/students/logout" method="post">
    <input type="submit" value="Logout" >
</form>
<table>
<tr>
    <td>Имя</td>
    <td>Дата рождения</td>
</tr>
<c:forEach items="${students}" var="studentItem">
    <tr id="<c:out  value="${studentItem.id}"></c:out>">
        <td><c:out value="${studentItem.name}"></c:out></td>
        <td><c:out value="${studentItem.birthdate}"></c:out></td>
        <td><a href="" onclick="deleteStudent(<c:out  value="${studentItem.id}"></c:out>)">Удалить</a></td>
        <td><a href="student?id=<c:out  value="${studentItem.id}"></c:out>">Редактировать</a></td>
    </tr>
</c:forEach>
</table>
<a href="student?id=0">Создать</a>
</body>
<script type="text/javascript" src="../../js/vendor/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="../../js/list.js"></script>
</html>
