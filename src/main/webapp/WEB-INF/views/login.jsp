<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page session="false" %>
<html>
<head>
    <title>Login</title>
</head>
<body>
<form name="f" action="/students/login" method="post">
    <label for="username">Login:</label>
    <input type="text" name="username" id="username" value=""><br/>
    <label for="password">Пароль:</label>
    <input type="password" name="password" id="password"><br/>
    <input type="submit" />
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
</form>
<a href="registration">Регистрация</a>

</body>
</html>
