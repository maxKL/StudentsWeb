<%@ page import="com.maximov.models.pojo.Lection" %><%--
  Created by IntelliJ IDEA.
  User: maxim_000
  Date: 24.02.2017
  Time: 10:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<% Lection lection = (Lection)request.getAttribute("lection"); %>
<form action="/students/lectionForm" method="post">

    <input hidden id="id" name="id" value="<%= lection.getId() %>"> <br/>

    <label for="name">Название: </label>
    <input id="name" name="name" value="<%= lection.getName() == null ? "" : lection.getName() %>"> <br/>
    <label for="subject">Тема: </label>
    <input id="subject" name="subject" value="<%= lection.getSubject() == null ? "" : lection.getSubject() %>"> <br/>
    <label for="text">Текст: </label>
    <input id="text" name="text" value="<%= lection.getText() == null ? "" : lection.getText() %>"> <br/>

    <input type="submit" value="Сохранить" />
</form>

</body>
</html>
